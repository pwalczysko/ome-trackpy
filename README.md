[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gh/ome/ome-trackpy/master?filepath=notebooks)

This guide demonstrates how to use [trackpy](https://github.com/soft-matter/trackpy) and OMERO.


To run the notebooks, you can either [run on mybinder.org](https://mybinder.org/v2/gh/ome/ome-trackpy/master?filepath=notebooks) or build locally with [repo2docker](https://repo2docker.readthedocs.io/).

To build locally:

 * Install [Docker](https://www.docker.com/) if required
 * Create a virtual environment and install repo2docker from PyPI.
 * Clone this repository
 * Run ``repo2docker``. 
 * Depending on the permissions, you might have to run the command as an admin


```
pip install jupyter-repo2docker
git clone https://github.com/ome/ome-trackpy.git
cd ome-trackpy
repo2docker .
```

To run the scripts:
 * Go in the ``ome-trackpy`` directory i.e. ``cd ome-trackpy``.
 * Install the dependencies using conda. Run
   * ``conda env create -f binder/environment.yml``. 
 * On the right-hand side of the terminal, click on the button ``New``
 * In the drop-down menu, select ``Terminal``.
 * In the terminal, ativate the various dependencies: ``conda activate notebook``
 * Go the ``scripts`` directory i.e. ``cd scripts``.
 * To run one of the tracking scripts on an image run:
    * ``python tracking_ome.py 58622 --type image``
    * supported types are ``dataset``, ``image`` and ``plate`` (default)
